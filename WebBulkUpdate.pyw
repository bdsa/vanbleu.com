#!/usr/bin/env python
'''
Bulk Update Web Stock using a CSV
'''

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import pypyodbc
import csv

conn = pypyodbc.connect("DSN=ASA")
cursor = conn.cursor()

class WebBulkUpdate:
    
    def __init__(self, parent=None):
        self.window = gtk.Window()
        try:
            self.window.set_screen(parent.get_screen())
        except AttributeError:
            self.window.connect('destroy', lambda *w: gtk.main_quit())
        self.window.set_title("Web Bulk Update")

        self.window.set_border_width(8)
        self.window.set_default_size(300,200)

        vbox =gtk.VBox(False,8)
        self.window.add(vbox)

        button = gtk.Button("Select CSV")
        button.connect("clicked", self.opendialog)
        button.show

        label = gtk.Label('''
<b>INSTRUCTIONS</b>

Prepare a CSV with 4 columns:

Stock Code
Short Name - will be truncated to 80 characters, used on website.
Long Name - will be truncated to 255 characters, used on instrument pricelists, usually the same as Short Name.
Description - use "&lt;br /&gt;" for line breaks.

The columns must be <b>IN THE ABOVE ORDER</b> with <b>NO HEADER ROW.</b>

Example:

<tt>VALT1.5,Vandoren Traditional alto saxophone reeds,Vandoren Traditional alto saxophone reeds,"This&lt;br /&gt;is the description."</tt>

Select one of the options below - this active/inactive setting only applies to wwr.co.uk.

Click "Select CSV" and find your CSV. When you click OK, the CSV will be processed.

Changes will only be committed to the database if no errors are encountered.
''')
        label.set_use_markup(True)

        radio1 = gtk.RadioButton(None, "Maintain active/inactive setting for existing products in the CSV. New products will be inactive.")
        radio1.connect("toggled", self.radio_selection_changed, "existing")

        radio2 = gtk.RadioButton(radio1, "Make all products in the CSV active.")
        radio2.connect("toggled", self.radio_selection_changed, "active")
                
        radio3 = gtk.RadioButton(radio1, "Make all products in the CSV inactive.")
        radio3.connect("toggled", self.radio_selection_changed, "inactive")
               
        radio1.set_active(True)
        radio1.toggled()

        vbox.pack_start(label, expand=True, fill=True)
        vbox.pack_start(radio1, expand=False)
        vbox.pack_start(radio2, expand=False)
        vbox.pack_start(radio3, expand=False)
        vbox.pack_start(button, expand=False, fill=False)
        vbox.show()

        self.window.show_all()

    def radio_selection_changed(self, w, data=None):
        self.activeradiobutton = data

    def opendialog(self, w, data=None):
        dialog = gtk.FileChooserDialog( title="Select a CSV",
                                        parent=self.window,
                                        action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                        buttons=("OK",True,"Cancel",False)
                                        )

        response = dialog.run()
        if response:
            filename = dialog.get_filename()
            self.readcsv(filename)    
        else:
            print "Cancelled"
        dialog.destroy()

    def readcsv(self, filename):
        with open(filename, 'r') as f:
            reader = csv.reader(f)
            rowcount = 0
            stockcodes_not_found = ""
            for row in reader:
                try:
                    stockcode = row[0]
                    shortname = row[1][:80]
                    longname = row[2][:255]
                    description = row[3]
                except:
                    errormsg = "Error - please check your CSV format."
                    md = gtk.MessageDialog(self.window,
                                gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING,
                                gtk.BUTTONS_CLOSE, errormsg)
                    md.run()
                    md.destroy()
                    return

                # is the stockcode in the database?
                cursor.execute("SELECT STKL_STKCODE FROM F_SM1_STOCK WHERE STKL_STKCODE = ?",
                                [stockcode])
                try:
                    realstockcode=cursor.fetchone()[0]
                except:                
                    #errormsg = "Error - stock code " + stockcode + " not found in Montana."
                    #md = gtk.MessageDialog(self.window,
                    #            gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING,
                    #            gtk.BUTTONS_CLOSE, errormsg)
                    #md.run()
                    #md.destroy()
                    #return
                    stockcodes_not_found += stockcode + ", "
                    continue

                # what's the active/inactive option set to? is the item in the web table and "Active on web"?
                if self.activeradiobutton == "existing":
                    cursor.execute("SELECT WTK_ACTIVE FROM WEB_STOCK WHERE WTK_STKCODE = ?",
                                [stockcode])
                    try:
                        active=cursor.fetchone()[0]
                    # doesn't exist? make inactive
                    except:
                        active=0
                elif self.activeradiobutton == "active":
                    active=1
                elif self.activeradiobutton == "inactive":
                    active=0
                
                    
                                        
                # update this stock item
                try:
                    cursor.execute("{call web_InsertUpdate_stock(?,?,?,?,?,0,0,0,0,0,?,?,?,3,0)}",
                                    [stockcode, shortname, longname, description, active, shortname, shortname, shortname])
                except:
                    md = gtk.MessageDialog(self.window,
                                gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING,
                                gtk.BUTTONS_CLOSE, "Error - please check your CSV format.")
                    md.run()
                    md.destroy()
                    return

                # increment row counter
                rowcount += 1

        conn.commit()
        infomsg = str(rowcount) + " rows updated."
        md = gtk.MessageDialog(self.window,
                    gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO,
                    gtk.BUTTONS_CLOSE, infomsg)
        md.run()
        md.destroy()

        errormsg = "Error - The following stock codes were not found in Montana:" + stockcodes_not_found
        md = gtk.MessageDialog(self.window,
                              gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING,
                              gtk.BUTTONS_CLOSE, errormsg)
        md.run()
        md.destroy()

            

def main():
    WebBulkUpdate()
    gtk.main()

if __name__ == '__main__':
    main()

        
