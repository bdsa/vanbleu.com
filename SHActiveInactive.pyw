#!/usr/bin/env python
'''
Make SH stock active or inactive.
'''

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import pypyodbc

conn = pypyodbc.connect("DSN=ASA")
cursor = conn.cursor()

class SHActiveInactive:
    
    def __init__(self, parent=None):
        self.window = gtk.Window()
        try:
            self.window.set_screen(parent.get_screen())
        except AttributeError:
            self.window.connect('destroy', lambda *w: gtk.main_quit())
        self.window.set_title("SH Active/Inactive")

        self.window.set_border_width(8)
        self.window.set_default_size(400,400)

        vbox = gtk.VBox(False,8)
        self.window.add(vbox)

        toprow = gtk.HBox(False, 8)
        
        self.searchbox = gtk.Entry()
        self.searchbox.set_max_length(50)
        self.searchbox.set_activates_default(True)
        self.searchbox.set_text("SH-")
                
        searchbtn = gtk.Button("Search")
        searchbtn.set_can_default(True)
        searchbtn.connect('clicked', self.search)
                
        toprow.pack_start(self.searchbox, expand=True, fill=True)
        toprow.pack_end(searchbtn, False, False, 0)
        self.searchbox.show()
        searchbtn.show()

        vbox.pack_start(toprow, False)

        self.button_make_active = gtk.Button("Make Active")
        self.button_make_active.connect("clicked", self.activate)
        self.button_make_active.set_sensitive(False)
        self.button_make_active.show
        
        self.button_make_inactive = gtk.Button("Make Inactive")
        self.button_make_inactive.connect("clicked", self.deactivate)
        self.button_make_inactive.set_sensitive(False)
        self.button_make_inactive.show

        self.label = gtk.Label('''
<big><b>Please enter a S/H stock code in the search box above\nand use the buttons below to make it active or inactive.\nChanges take effect immediately.</b></big>
''')
        self.label.set_use_markup(True)

        vbox.pack_start(self.label, expand=True, fill=True)
        vbox.pack_start(self.button_make_active, expand=False, fill=False)
        vbox.pack_start(self.button_make_inactive, expand=False, fill=False)
        vbox.show()

        self.window.set_default(searchbtn)
        self.window.show_all()

    def search(self, widget):
        stockcode = self.searchbox.get_text().upper()
        cursor.execute("SELECT COUNT(*) FROM F_SM1_STOCK WHERE STKL_STKCODE LIKE ?",
                       [stockcode])
        rows_found = cursor.fetchone()[0]
        if rows_found != 1:
            errormsg = "Please enter a specific SH stock code."
            md = gtk.MessageDialog(self.window,
                           gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING,
                           gtk.BUTTONS_CLOSE, errormsg)
            md.run()
            md.destroy()
            return
        cursor.execute("SELECT STKL_STKCODE, STKL_SHORTNAME, STKL_INACTIVE FROM F_SM1_STOCK WHERE STKL_STKCODE LIKE ?",
                       [stockcode])
        stockitem = cursor.fetchone()
        stockname = stockitem[1]
        inactive = stockitem[2]
        if inactive == 1:
            self.label.set_label("<big>" + stockcode + " is <b>inactive</b>.\n\n" + stockname + "</big>")
            self.button_make_active.set_sensitive(True)
            self.button_make_inactive.set_sensitive(False)
        if inactive == 0:
            self.label.set_label("<big>" + stockcode + " is <b>active</b>.\n\n" + stockname + "</big>")
            self.button_make_active.set_sensitive(False)
            self.button_make_inactive.set_sensitive(True)

    def activate(self, widget):
        stockcode = self.searchbox.get_text().upper()
        cursor.execute("UPDATE F_SM1_STOCK SET STKL_INACTIVE = 0 WHERE STKL_STKCODE = ?",
                       [stockcode])
        conn.commit()
        self.search(None)

    def deactivate(self, widget):
        stockcode = self.searchbox.get_text().upper()
        cursor.execute("UPDATE F_SM1_STOCK SET STKL_INACTIVE = 1 WHERE STKL_STKCODE = ?",
                       [stockcode])
        conn.commit()
        self.search(None)

def main():
    SHActiveInactive()
    gtk.main()

if __name__ == '__main__':
    main()

        
