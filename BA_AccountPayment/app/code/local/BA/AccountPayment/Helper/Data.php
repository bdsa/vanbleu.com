<?php

class BA_AccountPayment_Helper_Data extends Mage_Core_Helper_Abstract
{
  function getPaymentGatewayUrl() 
  {
    return Mage::getUrl('accountpayment/payment/gateway', array('_secure' => false));
  }
}