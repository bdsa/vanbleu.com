<?php

class BA_AccountPayment_Block_Info_Accountpayment extends Mage_Payment_Block_Info
{
  protected function _prepareSpecificInformation($transport = null)
  {
    if (null !== $this->_paymentSpecificInformation) 
    {
      return $this->_paymentSpecificInformation;
    }
     
    $data = array();
    if ($this->getInfo()->getPONumber()) 
    {
      $data[Mage::helper('payment')->__('PO Number')] = $this->getInfo()->getPONumber();
    }

    $transport = parent::_prepareSpecificInformation($transport);
     
    return $transport->setData(array_merge($data, $transport->getData()));
  }
}