<?php

class BA_AccountPayment_Block_Form_AccountPayment extends Mage_Payment_Block_Form
{
  protected function _construct()
  {
    parent::_construct();
    $this->setTemplate('accountpayment/form/accountpayment.phtml');
  }
}