<?php

class BA_AccountPayment_Model_Observer
{
  public function implementOrderStatus($event)
  {
    $order = $event->getOrder();

    if ($this->_getPaymentMethod($order) == 'accountpayment') {
      if ($order->canInvoice())
        $this->_processOrderStatus($order);
    }
    return $this;
  }

  private function _getPaymentMethod($order)
  {
    return $order->getPayment()->getMethodInstance()->getCode();
  }

  private function _processOrderStatus($order)
  {
    $invoice = $order->prepareInvoice();

    $invoice->register();
    Mage::getModel('core/resource_transaction')
      ->addObject($invoice)
      ->addObject($invoice->getOrder())
      ->save();

    $this->_changeOrderStatus($order);
    return true;
  }

  private function _changeOrderStatus($order)
  {
    $statusMessage = 'Set to Invoiced automatically - Account Payment';
    $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
    $order->save();
  }
}