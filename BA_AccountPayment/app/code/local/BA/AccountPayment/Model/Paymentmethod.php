<?php

class BA_AccountPayment_Model_Paymentmethod extends Mage_Payment_Model_Method_Abstract {
  protected $_code  = 'accountpayment';
  protected $_formBlockType = 'accountpayment/form_accountpayment';
  protected $_infoBlockType = 'accountpayment/info_accountpayment';
 
  public function assignData($data)
  {
    $info = $this->getInfoInstance();
     
    if ($data->getPoNumber())
    {
      $info->setPoNumber($data->getPoNumber());
    }
    
    return $this;
  }
 
  public function validate()
  {
    parent::validate();
    $info = $this->getInfoInstance();
     
    if (!$info->getPoNumber())
    {
      $errorCode = 'invalid_data';
      $errorMsg = $this->_getHelper()->__("Please enter a Purchase Order number or reference.\n");
    }
 
    if ($errorMsg) 
    {
      Mage::throwException($errorMsg);
    }
 
    return $this;
  }
 
  public function getOrderPlaceRedirectUrl()
  {
    // return Mage::getUrl('accountpayment/payment/redirect', array('_secure' => false));
  }
}