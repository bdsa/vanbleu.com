<?php

//$allowedIP[0]="213.123.248.66"; 
//$allowedIP[1]="81.136.245.62";

//if (!in_array($_SERVER['REMOTE_ADDR'],$allowedIP)) { echo "Unauthorised"; exit; };

function SendAck() {
 	$acknowledgment = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
     			            <acknowledge>
   	      			        <status>Recieved Thank you</status>
                 			</acknowledge>";
  echo $acknowledgment;
}

error_reporting ( E_ALL & ~E_NOTICE );

  define('RESPONSE_HANDLER_LOG_FILE', 'montanaresponse.log');

 //Setup the log file
  if (!$message_log = fopen(RESPONSE_HANDLER_LOG_FILE, "a")) {
    error_func("Cannot open " . RESPONSE_HANDLER_LOG_FILE . " file.\n", 0);
    exit(1);
  }


fwrite($message_log, sprintf("\n\r%s:- %s\n",date("D M j G:i:s T Y"),
      "despatch called"));
	  
	  
$xml_response = $_POST['xml'];

fwrite($message_log, sprintf("\n\r%s:- %s\n",date("D M j G:i:s T Y"),
      $xml_response));

if (get_magic_quotes_gpc()) {
  $xml_response = stripslashes($xml_response);
}

$xml = new SimpleXMLElement($xml_response);

//print_r($xml);
  
foreach ($xml->Dispatch as $values) {
	//print_r($values); 
		
	$orders[trim($values->ReedsDirectOrderRef)][trim($values->StockCode)] =  $values->QtyDispatched;
	
}

/*
$orders ['100000016']['LVAH'] = 1;
$orders ['100000016']['RBASS3'] = 1;
$orders ['100000016']['MPALT3.5'] = 1;
$orders ['100000015']['LALT3.75'] = 1;
$orders ['100000014']['MPALT3.5'] = 2;
$orders ['100000014']['AC10A1.5'] = 5;
*/
print_r($orders);

include_once '../app/Mage.php';
Mage::app();

foreach ($orders as $key => $value) {
	
	$order = Mage::getModel('sales/order')->loadByIncrementId($key);
	foreach ($value as $sku => $productDispached) {
		$products [] = $sku;
		$qtyDispatched[$sku] =  $productDispached;
	}

	$items = $order->getItemsCollection();
	$qtys = array(); 		

	foreach($items as $item){
		if($item->getPrice()>0){ 
		  if(in_array($item->getSku(),$products)){
				if (strcmp($item->getStatus(),"Shipped")!=0){
					$qty_to_invoice = $qtyDispatched[$item->getSku()];
			  	$qtys[$item->getId()] = $qty_to_invoice;							
				}
		  }
		  else{

		 		$qty_to_invoice = '0';
		  	$qtys[$item->getId()] = $qty_to_invoice;
			}
	  }	   		
  }
	

	$i = 0;
	foreach ($qtys as $key => $value) {		
		if($value>0)
			$i++;
	}

	if($i){
		$shipment = $order->prepareShipment($qtys);
		if ($shipment) {
	    $shipment->register();
	   	$shipment->sendEmail(true)->setEmailSent(true)->save();
			$history = $shipment->getOrder()->addStatusHistoryComment('Partial order automatically.', false);
	   	$history->setIsCustomerNotified(true);
	   	$transactionSave = Mage::getModel('core/resource_transaction')->addObject($shipment)->addObject($shipment->getOrder())->save();                
	  }
	}
}

SendAck();

?>