<?php

include_once '../app/Mage.php';
Mage::app();


header("Content-Type: application/xml; charset=UTF-8"); 
$xmloutput = '<?xml version="1.0" encoding="UTF-8"?>
<Orders>';

$ordersProcessing = Mage::getModel('sales/order')->getCollection()->addAttributeToFilter('status', array('eq' => Mage_Sales_Model_Order::STATE_PROCESSING));
foreach ($ordersProcessing as $key => $value) {
    $IncrementId = $value->getIncrementId();
    $order = Mage::getModel('sales/order')->loadByIncrementId($value->getIncrementId());
    $ordersDetails[$key] = $order->getData();

}


foreach ($ordersDetails as $key => $value) {
    
    if ($value['tax_amount']>0)
        $VatExempt = 0; 
    else
        $VatExempt = 1;
	
    $order = Mage::getModel('sales/order')->loadByIncrementId($value['increment_id']);
    $payment = $order->getPayment();
    $paymentMethod = $payment->getMethod();
    $ponumber = $payment->getPoNumber();
    $customercomments = $order->getGomageCheckoutCustomerComment();
		
		switch ($paymentMethod) {
		case 'sagepaydirectpro':
			$myCard = "SAGEPAY";
			$paymentMethod='Secure Credit Card'; 
			break;
		case 'accountpayment':
			$myCard = "ONACCOUNT";
			break;
		case 'paypal_standard':
			$paymentMethod='Paypal Express';
			$myCard = "PAYPAL";	 
		}

    if (isset($ponumber)) {
        $customercomments = "PO Number: " . $ponumber . " Customer Comments: " . $customercomments;
    }

    $xmloutput .= ' 
        <Order>
            <Header>
                <Reference>'.$value['increment_id'].'</Reference>
                <OrderTotal>'.$value['base_grand_total'].'</OrderTotal>
                <DatePlaced>'.$value['created_at'].'</DatePlaced>
                <Status>0</Status>  
                <PublicNotes><![CDATA[]]> </PublicNotes>   
                <PrivateNotes><![CDATA[]]></PrivateNotes>  
                <ShippingMethod><![CDATA['.$value['shipping_method'].']]></ShippingMethod>
                <DiscountCode><![CDATA[]]>'.$value['coupon_code'].'</DiscountCode>
                <DiscountAmount>'.$value['base_discount_amount'].'</DiscountAmount>
                <ShippingCost>'.$value['shipping_amount'].'</ShippingCost>
                <AdjustmentValue></AdjustmentValue>
                <AdjustmentReason></AdjustmentReason>
                <CardType>'.$myCard.'</CardType>          
                <CardNumber></CardNumber>            
                <CardStartMonth></CardStartMonth>            
                <CardStartYear></CardStartYear>           
                <CardExpiryMonth></CardExpiryMonth>         
                <CardExpiryYear></CardExpiryYear>            
                <CardIssue></CardIssue>           
                <CardName></CardName>          
                <CardVerify></CardVerify>            
                <CustomerComments>'.$customercomments.'</CustomerComments>  
                <VatExempt>'.$VatExempt.'</VatExempt>
            </Header>';   
      
    
    /*******************************************************/


    $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($value['customer_email']);
    if($subscriber->getId())
       $mailingList = "Y";
    else
       $mailingList = "N"; 

    $address = Mage::getModel("sales/order")->load($key); //load order by order id 
    $billing_address = $address->getBillingAddress(); 
    $Shipping_address = $address->getShippingAddress(); 
    $billingAddress = $billing_address->getData();
    $ShippingAddress = $Shipping_address->getData();

    $countryModel = Mage::getModel('directory/country')->loadByCode($billingAddress['country_id']);
    $billingcountryName = $countryModel->getName();

    $countryModel = Mage::getModel('directory/country')->loadByCode($ShippingAddress['country_id']);
    $ShippingcountryName = $countryModel->getName();
   

    //print_r($ShippingAddress);
   
 
    if (!(strlen($value['customer_id'])>0)){

        $MKID = $value['increment_id'];
        $Created = "";
        $Email = $value['customer_email'];
        $DatePlaced = "";
        $Company = $billingAddress['company'];

    }
      else{
        
        $MKID = $value['customer_id'];
        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        $customer = $customer->getData();
        $Created =  $customer['created_at'];
        $Email = $customer['email'];
        $DatePlaced = $customer['created_at'];
        $Company = $billingAddress['company'];

    }

		$street = explode("\n", $billingAddress['street']);
		$dlystreet = explode("\n", $ShippingAddress['street']);

    $xmloutput .= '     
            <Customer>          
                <MKID>'.$MKID.'</MKID>           
                    <Created>'.$Created.'</Created>         
                    <Email><![CDATA['.$Email.']]></Email>           
                    <Status>A</Status>          
                    <DefaultPaymentType>'.$paymentMethod.'</DefaultPaymentType>            
                    <DatePlaced>'.$Created.'</DatePlaced>           
                    <MailingList>'.$mailingList.'</MailingList>         
                    <Company><![CDATA['.$billingAddress['company'].']]></Company>           
                    <InvoiceAddress>                
                        <Title></Title>               
                        <FirstName><![CDATA['.$billingAddress['firstname'].']]></FirstName>               
                        <LastName><![CDATA[.'.$billingAddress['lastname'].']]></LastName>              
                        <Phone><![CDATA['. $billingAddress['telephone'].']]></Phone>               
                        <Address1><![CDATA['.$street[0].']]></Address1>                
                        <Address2><![CDATA['.$street[1].']]></Address2>                
                        <City><![CDATA['.$street[2].']]></City>              
                        <Postcode><![CDATA['.$billingAddress['postcode'].']]></Postcode>               
                        <County><![CDATA['.$billingAddress['city'].']]></County>                
                        <Country><![CDATA['.$billingcountryName.']]></Country>          
                    </InvoiceAddress>
                    <DeliveryAddress>               
                        <Title></Title>                
                        <FirstName><![CDATA['.$ShippingAddress['firstname'].']]></FirstName>                
                        <LastName><![CDATA['.$ShippingAddress['lastname'].']]></LastName>               
                        <Phone><![CDATA['.$ShippingAddress['telephone'].']]></Phone>              
                        <Address1><![CDATA['.$dlystreet[0].']]></Address1>                
                        <Address2><![CDATA['.$dlystreet[1].']]></Address2>                
                        <City><![CDATA['.$dlystreet[2].']]></City>               
                        <Postcode><![CDATA['.$ShippingAddress['postcode'].']]></Postcode>                
                        <County><![CDATA['. $ShippingAddress['city'].']]></County>             
                        <Country><![CDATA['. $ShippingcountryName.']]></Country>          
                    </DeliveryAddress>     
            </Customer>';

    /*******************************************************/

    $xmloutput .= '         
            <Detail>';
    $line = Mage::getModel("sales/order")->load($key); 
    $ordered_items = $line->getAllItems(); 
    Foreach($ordered_items as $item){      
      if($item->getPrice()>0){
        $xmloutput.='
                <Line>
                    <StockCode><![CDATA['. $item->getSku().']]></StockCode>
                    <Quantity>'.$item->getQtyOrdered().'</Quantity>
                    <Price>'.$item->getPriceInclTax().'</Price>
                </Line>';
      } 
       
    } 
    $xmloutput .= '
            </Detail>    
    </Order>';

} 
$xmloutput .= '
</Orders>';

echo $xmloutput; 
  
    


?>