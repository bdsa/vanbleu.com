#!/usr/bin/env python
'''
Bulk Update Web Stock using a CSV
'''

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import WebBulkUpdate
import WebStockEditor
import PriceEditor
import StockNameEditor
import SHActiveInactive

class StockEditors:
    
    def __init__(self, parent=None):
        self.window = gtk.Window()
        try:
            self.window.set_screen(parent.get_screen())
        except AttributeError:
            self.window.connect('destroy', lambda *w: gtk.main_quit())
        self.window.set_title("Stock Editors")

        self.window.set_border_width(8)
        self.window.set_default_size(250,100)

        vbox =gtk.VBox(False,8)
        self.window.add(vbox)

        button = gtk.Button("Web Stock Editor")
        button.connect("clicked", self.webstockeditor)
        button.show

        button2 = gtk.Button("Stock Name Editor")
        button2.connect("clicked", self.stocknameeditor)
        button2.show

        button3 = gtk.Button("Price Editor")
        button3.connect("clicked", self.priceeditor)
        button3.show

        button4 = gtk.Button("Web Bulk Update")
        button4.connect("clicked", self.webbulkupdate)
        button4.show

        button5 = gtk.Button("Secondhand - Active or Inactive")
        button5.connect("clicked", self.shactiveinactive)
        button5.show
        
        separator = gtk.HSeparator()
        separator.show

        vbox.pack_start(button, expand=False, fill=False)
        vbox.pack_start(button2, expand=False, fill=False)
        vbox.pack_start(button3, expand=False, fill=False)
        vbox.pack_start(button5, expand=False, fill=False)
        vbox.pack_start(separator)
        vbox.pack_start(button4, expand=False, fill=False)
        vbox.show()

        self.window.show_all()

    def webstockeditor(self, parent=None):
        WebStockEditor.main()

    def stocknameeditor(self, parent=None):
        StockNameEditor.main()

    def priceeditor(self, parent=None):
        PriceEditor.main()

    def webbulkupdate(self, parent=None):
        WebBulkUpdate.main()

    def shactiveinactive(self, parent=None):
        SHActiveInactive.main()

def main():
    StockEditors()
    gtk.main()

if __name__ == '__main__':
    main()

        
