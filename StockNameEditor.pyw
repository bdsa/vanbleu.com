#!/usr/bin/env python
'''
Edit Web Stock
'''

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import pypyodbc

conn = pypyodbc.connect("DSN=ASA")
cursor = conn.cursor()
rows = cursor.execute("SELECT F_SM1_STOCK.STKL_STKCODE, F_SM1_STOCK.STKL_STKNAME, WEB_STOCK.WTK_SHORT FROM F_SM1_STOCK LEFT OUTER JOIN WEB_STOCK ON F_SM1_STOCK.STKL_STKCODE=WEB_STOCK.WTK_STKCODE WHERE F_SM1_STOCK.STKL_STKCODE LIKE ? AND STKL_INACTIVE=0",
                      ['NOTASTOCKCODE'])
        
(
    COLUMN_STOCKCODE,
    COLUMN_MONNAME,
    COLUMN_WEBSHORT,
) = range(3)

class WebStockEditor(gtk.Window):
    def __init__(self, parent=None):
        gtk.Window.__init__(self)
        try:
            self.set_screen(parent.get_screen())
        except AttributeError:
            self.connect('destroy', lambda *w: gtk.main_quit())
        self.set_title("Stock Name Editor")

        self.set_border_width(8)
        self.set_default_size(800,600)

        toprow = gtk.HBox(False, 8)
        
        self.searchbox1 = gtk.Entry()
        self.searchbox1.set_max_length(50)
        self.searchbox1.set_activates_default(True)
        self.searchbox1.set_text("Stock Code")
        
        self.searchbox2 = gtk.Entry()
        self.searchbox2.set_max_length(100)
        self.searchbox2.set_activates_default(True)
        self.searchbox2.set_text("Name")
        
        searchbtn = gtk.Button("Search")
        searchbtn.set_can_default(True)
        searchbtn.connect('clicked', self.search, self.searchbox1)
                
        toprow.pack_start(self.searchbox1, expand=True, fill=True)
        toprow.pack_start(self.searchbox2, expand=True, fill=True)
        toprow.pack_end(searchbtn, False, False, 0)
        self.searchbox1.show()
        self.searchbox2.show()
        searchbtn.show()

        toprow2 = gtk.HBox(False, 8)
        instructions = gtk.Label("Search for stock code, name or both. Use * as a wildcard to represent multiple characters.")
        toprow2.pack_start(instructions, True, True)
        instructions.show()

        vbox = gtk.VBox(False, 8)
        self.add(vbox)

        vbox.pack_start(toprow, False)
        vbox.pack_start(toprow2, False)

        sw = gtk.ScrolledWindow()
        sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        vbox.pack_start(sw)

        self.model = self.__create_model(rows)

        global treeview
        treeview = gtk.TreeView(self.model)
        treeview.set_rules_hint(True)
        treeview.set_search_column(COLUMN_STOCKCODE)

        gtk.rc_parse_string( """
        style "custom-treestyle"{
            GtkTreeView::odd-row-color = "#ededed"
            GtkTreeView::even-row-color = "#ffffff"
            GtkTreeView::allow-rules = 1
        }
        widget "*custom_treeview*" style "custom-treestyle"
        """)
        treeview.set_name("custom_treeview" )
        
        

        sw.add(treeview)

        self.__add_columns(treeview)

        self.maximize()
        self.set_default(searchbtn)
        self.show_all()

    def search(self, widget, searchbox):
        searchterm1 = self.searchbox1.get_text()
        sqlsearchterm1 = searchterm1.replace("*","%")
        searchterm2 = self.searchbox2.get_text()
        sqlsearchterm2 = searchterm2.replace("*","%")
        qq_sqlsearchterm1 = "qq_" + sqlsearchterm1
        
        if (searchterm1 <> "" and searchterm1 <> "Stock Code") and (searchterm2 == "" or searchterm2 == "Name"):
            rows = cursor.execute("SELECT F_SM1_STOCK.STKL_STKCODE, F_SM1_STOCK.STKL_STKNAME, WEB_STOCK.WTK_SHORT FROM F_SM1_STOCK LEFT OUTER JOIN WEB_STOCK ON F_SM1_STOCK.STKL_STKCODE=WEB_STOCK.WTK_STKCODE WHERE STKL_INACTIVE=0 AND (F_SM1_STOCK.STKL_STKCODE LIKE ? OR F_SM1_STOCK.STKL_STKCODE LIKE ?) ORDER BY F_SM1_STOCK.STKL_SORT1, F_SM1_STOCK.STKL_STKCODE",
                                  [sqlsearchterm1, qq_sqlsearchterm1])
        if (searchterm1 == "" or searchterm1 == "Stock Code") and (searchterm2 <> "" and searchterm2 <> "Name"):
            rows = cursor.execute("SELECT F_SM1_STOCK.STKL_STKCODE, F_SM1_STOCK.STKL_STKNAME, WEB_STOCK.WTK_SHORT FROM F_SM1_STOCK LEFT OUTER JOIN WEB_STOCK ON F_SM1_STOCK.STKL_STKCODE=WEB_STOCK.WTK_STKCODE WHERE STKL_INACTIVE=0 AND F_SM1_STOCK.STKL_STKNAME LIKE ? ORDER BY F_SM1_STOCK.STKL_SORT1, F_SM1_STOCK.STKL_STKCODE",
                                  [sqlsearchterm2])
        if (searchterm1 <> "" and searchterm1 <> "Stock Code") and (searchterm2 <> "" and searchterm2 <> "Name"):
            rows = cursor.execute("SELECT F_SM1_STOCK.STKL_STKCODE, F_SM1_STOCK.STKL_STKNAME, WEB_STOCK.WTK_SHORT FROM F_SM1_STOCK LEFT OUTER JOIN WEB_STOCK ON F_SM1_STOCK.STKL_STKCODE=WEB_STOCK.WTK_STKCODE WHERE STKL_INACTIVE=0 AND (F_SM1_STOCK.STKL_STKCODE LIKE ? OR F_SM1_STOCK.STKL_STKCODE LIKE ?) AND F_SM1_STOCK.STKL_STKNAME LIKE ? ORDER BY F_SM1_STOCK.STKL_SORT1, F_SM1_STOCK.STKL_STKCODE",
                                  [sqlsearchterm1, qq_sqlsearchterm1, sqlsearchterm2])

        self.model = self.__create_model(rows)
        treeview.set_model(self.model)

    def __create_model(self, rows):
        lstore = gtk.ListStore(
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING)

        lstore.clear()

        for item in rows:
            iter = lstore.append()     
            lstore.set(iter,
                COLUMN_STOCKCODE, item[0],
                COLUMN_MONNAME, item[1],
                COLUMN_WEBSHORT, item[2])
        return lstore

    def cell_edited_callback(self, renderer, row, new_value, column, edited_column_id, edited_column_name):
        if new_value == "":
            return
        stockcode = self.model[row][column]
        current_monname = self.model[row][1]
        current_webshort = self.model[row][2]
        new_value_80 = new_value[:80]
       # if edited_column_name == 'WTK_SHORT':
       #     params = [stockcode, new_value_80, current_long, current_desc, new_value_80, new_value_80, new_value_80]
        current_value = self.model[row][edited_column_id]
        
        if new_value <> current_value:
            cursor.execute("UPDATE F_SM1_STOCK SET STKL_STKNAME = ? WHERE STKL_STKCODE = ?",
                           [new_value, stockcode])
            cursor.execute("DELETE FROM KEYWORD_SEARCH WHERE KWS_STKCODE = ?",
                           [stockcode])
            cursor.execute("exec mbo_stock_insert_keywords(?)",
                           [stockcode])
            conn.commit()
            self.search('', self.searchbox1)
                        
    def __add_columns(self,treeview):
        self.model = treeview.get_model()

        self.renderer1 = gtk.CellRendererText()
        self.renderer1.connect('edited', self.cell_edited_callback, 0, 1, "STKL_STKNAME")
        self.renderer1.set_property('editable', True)

        #self.renderer2 = gtk.CellRendererText()
        #self.renderer2.connect('edited', self.cell_edited_callback, 0, 2, "WTK_SHORT")
        #self.renderer2.set_property('editable', True)
                                
        column = gtk.TreeViewColumn('Stock Code', gtk.CellRendererText(),
                                    text=COLUMN_STOCKCODE)
        column.set_sort_column_id(COLUMN_STOCKCODE)
        treeview.append_column(column)

        column = gtk.TreeViewColumn('Montana Name', self.renderer1,
                                    text=COLUMN_MONNAME)
        column.set_sort_column_id(COLUMN_MONNAME)
        column.set_resizable(True)
        treeview.append_column(column)
        
        #column = gtk.TreeViewColumn('Web Short Name', self.renderer2,
        column = gtk.TreeViewColumn('Web Short Name', gtk.CellRendererText(),
                                    text=COLUMN_WEBSHORT)
        column.set_sort_column_id(COLUMN_WEBSHORT)
        column.set_resizable(True)
        treeview.append_column(column)
        
def main():
    WebStockEditor()
    gtk.main()

if __name__ == '__main__':
    main()

    


        
