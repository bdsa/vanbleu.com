<?php
class BA_GroupPaymentRestriction_Model_Observer {

	public function filterpaymentmethod(Varien_Event_Observer $observer) {

		$event = $observer->getEvent();
		$method = $event->getMethodInstance();
		$result = $event->getResult();


		if($method->getCode()=='accountpayment'){
			$result->isAvailable = false;
			
			if(Mage::getSingleton('customer/session')->isLoggedIn()) {
				$roleId = Mage::getSingleton('customer/session')->getCustomerGroupId();
				$role = Mage::getSingleton('customer/group')->load($roleId)->getData('customer_group_code');
				$role = strtolower($role);

				if($role=='wholesale'){
					$result->isAvailable = true;
				}
			}
		}
	}
}
?>