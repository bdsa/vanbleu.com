#!/usr/bin/env python
'''
Edit Web Stock
'''

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import pypyodbc

conn = pypyodbc.connect("DSN=ASA")
cursor = conn.cursor()
rows = cursor.execute("SELECT F_SM1_STOCK.STKL_STKCODE, WTK_SHORT, WTK_LONG, WTK_DESC FROM F_SM1_STOCK LEFT OUTER JOIN WEB_STOCK ON F_SM1_STOCK.STKL_STKCODE=WEB_STOCK.WTK_STKCODE WHERE F_SM1_STOCK.STKL_STKCODE LIKE ? AND STKL_INACTIVE=0",
                      ['NOTASTOCKCODE'])
        
(
    COLUMN_STOCKCODE,
    COLUMN_SHORT,
    COLUMN_LONG,
    COLUMN_DESC
) = range(4)

def rowcheckfunc(model, path, iter, stockcode):
    global selected_path
    if model.get_value(iter,0) == stockcode:
        selected_path = path

class WebStockEditor(gtk.Window):
    def __init__(self, parent=None):
        gtk.Window.__init__(self)
        try:
            self.set_screen(parent.get_screen())
        except AttributeError:
            self.connect('destroy', lambda *w: gtk.main_quit())
        self.set_title("Web Stock Editor")

        self.set_border_width(8)
        self.set_default_size(800,600)

        toprow = gtk.HBox(False, 8)
        
        self.searchbox1 = gtk.Entry()
        self.searchbox1.set_max_length(50)
        self.searchbox1.set_activates_default(True)
        self.searchbox1.set_text("Stock Code")
        
        self.searchbox2 = gtk.Entry()
        self.searchbox2.set_max_length(100)
        self.searchbox2.set_activates_default(True)
        self.searchbox2.set_text("Name")

        self.sort2combo = gtk.combo_box_new_text()
        self.sort2combo.append_text("All Categories")
        self.sort2combo.append_text("Accessory")
        self.sort2combo.append_text("Instrument")
        self.sort2combo.append_text("Mouthpiece")
        self.sort2combo.append_text("Reeds")
        self.sort2combo.set_active(0)
        
        searchbtn = gtk.Button("Search")
        searchbtn.set_can_default(True)
        searchbtn.connect('clicked', self.search, self.searchbox1)
                
        toprow.pack_start(self.searchbox1, expand=True, fill=True)
        toprow.pack_start(self.searchbox2, expand=True, fill=True)
        toprow.pack_end(self.sort2combo, False, False, 0)
        toprow.pack_end(searchbtn, False, False, 0)
        self.searchbox1.show()
        self.searchbox2.show()
        self.sort2combo.show()
        searchbtn.show()

        toprow2 = gtk.HBox(False, 8)
        instructions = gtk.Label("Search for stock code, name or both. Use * as a wildcard to represent multiple characters.")
        toprow2.pack_start(instructions, True, True)
        instructions.show()

        vbox = gtk.VBox(False, 8)
        self.add(vbox)

        vbox.pack_start(toprow, False)
        vbox.pack_start(toprow2, False)

        sw = gtk.ScrolledWindow()
        sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        vbox.pack_start(sw)

        self.model = self.__create_model(rows)

        global treeview
        treeview = gtk.TreeView(self.model)
        treeview.set_rules_hint(True)
        treeview.set_search_column(COLUMN_STOCKCODE)

        global selected_path

        gtk.rc_parse_string( """
        style "custom-treestyle"{
            GtkTreeView::odd-row-color = "#ededed"
            GtkTreeView::even-row-color = "#ffffff"
            GtkTreeView::allow-rules = 1
        }
        widget "*custom_treeview*" style "custom-treestyle"
        """)
        treeview.set_name("custom_treeview" )
        
        

        sw.add(treeview)

        self.__add_columns(treeview)

        self.maximize()
        self.set_default(searchbtn)
        self.show_all()

    def search(self, widget, searchbox):
        searchterm1 = self.searchbox1.get_text()
        sqlsearchterm1 = searchterm1.replace("*","%")
        searchterm2 = self.searchbox2.get_text()
        sqlsearchterm2 = searchterm2.replace("*","%")
        qq_sqlsearchterm1 = "qq_" + sqlsearchterm1
        sort2 = self.sort2combo.get_active_text()
        if sort2 == "All Categories":
            sort2 = "%"

        
        if (searchterm1 <> "" and searchterm1 <> "Stock Code") and (searchterm2 == "" or searchterm2 == "Name"):
            rows = cursor.execute("SELECT F_SM1_STOCK.STKL_STKCODE, WTK_SHORT, WTK_LONG, WTK_DESC FROM F_SM1_STOCK LEFT OUTER JOIN WEB_STOCK ON F_SM1_STOCK.STKL_STKCODE=WEB_STOCK.WTK_STKCODE WHERE STKL_INACTIVE=0 AND (F_SM1_STOCK.STKL_STKCODE LIKE ? OR F_SM1_STOCK.STKL_STKCODE LIKE ?) AND STKL_SORT2 LIKE ? ORDER BY F_SM1_STOCK.STKL_SORT1, F_SM1_STOCK.STKL_STKCODE",
                                  [sqlsearchterm1, qq_sqlsearchterm1, sort2])
        if (searchterm1 == "" or searchterm1 == "Stock Code") and (searchterm2 <> "" and searchterm2 <> "Name"):
            rows = cursor.execute("SELECT F_SM1_STOCK.STKL_STKCODE, WTK_SHORT, WTK_LONG, WTK_DESC FROM F_SM1_STOCK LEFT OUTER JOIN WEB_STOCK ON F_SM1_STOCK.STKL_STKCODE=WEB_STOCK.WTK_STKCODE WHERE STKL_INACTIVE=0 AND WEB_STOCK.WTK_SHORT LIKE ?  AND STKL_SORT2 LIKE ? ORDER BY F_SM1_STOCK.STKL_SORT1, F_SM1_STOCK.STKL_STKCODE",
                                  [sqlsearchterm2, sort2])
        if (searchterm1 <> "" and searchterm1 <> "Stock Code") and (searchterm2 <> "" and searchterm2 <> "Name"):
            rows = cursor.execute("SELECT F_SM1_STOCK.STKL_STKCODE, WTK_SHORT, WTK_LONG, WTK_DESC FROM F_SM1_STOCK LEFT OUTER JOIN WEB_STOCK ON F_SM1_STOCK.STKL_STKCODE=WEB_STOCK.WTK_STKCODE WHERE STKL_INACTIVE=0 AND (F_SM1_STOCK.STKL_STKCODE LIKE ? OR F_SM1_STOCK.STKL_STKCODE LIKE ?) AND WEB_STOCK.WTK_SHORT LIKE ?  AND STKL_SORT2 LIKE ? ORDER BY F_SM1_STOCK.STKL_SORT1, F_SM1_STOCK.STKL_STKCODE",
                                  [sqlsearchterm1, qq_sqlsearchterm1, sqlsearchterm2, sort2])

        self.model = self.__create_model(rows)
        treeview.set_model(self.model)

    def __create_model(self, rows):
        lstore = gtk.ListStore(
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING)

        lstore.clear()

        for item in rows:
            iter = lstore.append()     
            lstore.set(iter,
                COLUMN_STOCKCODE, item[0],
                COLUMN_SHORT, item[1],
                COLUMN_LONG, item[2],
                COLUMN_DESC, item[3])
        return lstore

    def cell_edited_callback(self, renderer, row, new_value, column, edited_column_id, edited_column_name):
        if new_value == "":
            return
        stockcode = self.model[row][column]
        cursor.execute("SELECT WTK_ACTIVE FROM WEB_STOCK WHERE WTK_STKCODE = ?",
                                [stockcode])
        try:
            active = cursor.fetchone()[0]
        except:
            active = 0
        current_desc = self.model[row][3]
        current_long = self.model[row][2]
        current_short = self.model[row][1]
        new_value_80 = new_value[:80]
        if edited_column_name == 'WTK_SHORT':
            params = [stockcode, new_value_80, current_long, current_desc, active, new_value_80, new_value_80, new_value_80]
        if edited_column_name == 'WTK_LONG':
            params = [stockcode, current_short, new_value, current_desc, active, current_short, current_short, current_short]
        if edited_column_name == 'WTK_SHORT' and current_long is None:
            params = [stockcode, new_value_80, new_value, new_value, active, new_value_80, new_value_80, new_value_80]
        if edited_column_name == 'WTK_LONG' and current_short is None:
            params = [stockcode, new_value_80, new_value, new_value, active, new_value_80, new_value_80, new_value_80]
        current_value = self.model[row][edited_column_id]
        
        if new_value <> current_value:
            cursor.execute("{call web_InsertUpdate_stock(?,?,?,?,?,0,0,0,0,0,?,?,?,3,0)}",
                              params)
            conn.commit()
            self.search('', self.searchbox1)
            self.model.foreach(rowcheckfunc, stockcode)
            treeview.set_cursor(selected_path)
  

    def save_callback(self, widget, new_desc_buffer, current_desc, current_short, current_long, stockcode):
        new_desc_with_returns = new_desc_buffer.get_text(*new_desc_buffer.get_bounds())
        new_desc = new_desc_with_returns.replace('\n','<br />').replace('\r','')
        if new_desc <> current_desc:
            cursor.execute("SELECT WTK_ACTIVE FROM WEB_STOCK WHERE WTK_STKCODE = ?",
                                [stockcode])
            active=cursor.fetchone()[0]
            cursor.execute("{call web_InsertUpdate_stock(?,?,?,?,?,0,0,0,0,0,?,?,?,3,0)}",
                           [stockcode, current_short, current_long, new_desc, active, current_short, current_short, current_short])
            conn.commit()
            self.search('', self.searchbox1)
            self.model.foreach(rowcheckfunc, stockcode)
            treeview.set_cursor(selected_path)
  
        self.desc_window.destroy()
        
    def row_activated_callback(self, renderer, row, view_column, desc_column, stkcode_column):
        current_desc = self.model[row][desc_column]
        stockcode = self.model[row][stkcode_column]
        current_long = self.model[row][2]
        current_short = self.model[row][1]

        if current_short is None or current_long is None:
            md = gtk.MessageDialog(self,
                                gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING,
                                gtk.BUTTONS_CLOSE, "Please set short and long names first.")
            md.run()
            md.destroy()
            return
                
        self.desc_window = gtk.Window()
        self.desc_window.set_title(stockcode + " - Web Description")
        self.desc_window.resize(900,500)
        
        desc_window_vbox = gtk.VBox()
        self.desc_window.add(desc_window_vbox)
        desc_window_bottomrow = gtk.HBox()
        
        desc_textview_sw = gtk.ScrolledWindow()
        desc_textview_sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        desc_textview = gtk.TextView()
        desc_textbuffer = desc_textview.get_buffer()
        if current_desc <> None:
            desc_textbuffer.set_text(current_desc)
        
        desc_cancel_button = gtk.Button("Cancel")
        desc_cancel_button.connect("clicked",lambda wid:self.desc_window.destroy())
        
        desc_save_button = gtk.Button("Save")
        desc_save_button.connect("clicked", self.save_callback, desc_textview.get_buffer(), current_desc, current_short, current_long, stockcode)
        
        desc_window_bottomrow.pack_start(desc_save_button, False, False)
        desc_window_bottomrow.pack_start(desc_cancel_button, False, False)

        desc_textview_sw.add(desc_textview)
        desc_window_vbox.pack_end(desc_textview_sw)
        desc_window_vbox.pack_start(desc_window_bottomrow, False, False)
        #self.desc_window.maximize()
        self.desc_window.show_all()
                        
    def __add_columns(self,treeview):
        self.model = treeview.get_model()

        self.renderer1 = gtk.CellRendererText()
        self.renderer1.connect('edited', self.cell_edited_callback, 0, 1, "WTK_SHORT")
        self.renderer1.set_property('editable', True)

        self.renderer2 = gtk.CellRendererText()
        self.renderer2.connect('edited', self.cell_edited_callback, 0, 2, "WTK_LONG")
        self.renderer2.set_property('editable', True)
                                
        column = gtk.TreeViewColumn('Stock Code', gtk.CellRendererText(),
                                    text=COLUMN_STOCKCODE)
        column.set_sort_column_id(COLUMN_STOCKCODE)
        treeview.append_column(column)

        column = gtk.TreeViewColumn('Short Name', self.renderer1,
                                    text=COLUMN_SHORT)
        column.set_sort_column_id(COLUMN_SHORT)
        column.set_resizable(True)
        column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
        column.set_fixed_width(250)
        column.set_min_width(10)
        treeview.append_column(column)
        
        column = gtk.TreeViewColumn('Long Name - only used on instrument pricelists and wwr.co.uk', self.renderer2,
                                    text=COLUMN_LONG)
        column.set_sort_column_id(COLUMN_LONG)
        column.set_resizable(True)
        column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
        column.set_fixed_width(250)
        column.set_min_width(10)
        treeview.append_column(column)
        
        column = gtk.TreeViewColumn('Description', gtk.CellRendererText(),
                                    text=COLUMN_DESC, markup=0)
        column.set_sort_column_id(COLUMN_DESC)

        treeview.connect('row-activated', self.row_activated_callback, 3, 0)
        treeview.append_column(column)



def main():
    WebStockEditor()
    gtk.main()

if __name__ == '__main__':
    main()

    


        
