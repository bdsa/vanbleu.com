#!/usr/bin/env python
'''
Edit PriceLines
'''

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import pypyodbc
import re

currencyre = re.compile("^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$")
conn = pypyodbc.connect("DSN=ASA")
cursor = conn.cursor()
rows = cursor.execute("SELECT Stock_SortKey1, Stock_Code, Stock_Name, List(LITE_SUPSTOCK.PSS_SUPREF,', ') AS Supplier_Stock_Codes, Stock_PriceLine1, Stock_PriceLine2, Stock_PriceLine3, Stock_PriceLine4 FROM Stock_Records JOIN LITE_SUPSTOCK ON Stock_Records.Stock_Code=LITE_SUPSTOCK.PSS_STKCODE WHERE Stock_Code LIKE ? AND Stock_Inactive_Flag = 'NO' GROUP BY Stock_Code, Stock_SortKey1, Stock_Name, Stock_PriceLine1, Stock_PriceLine2, Stock_PriceLine3, Stock_PriceLine4 ORDER BY Stock_SortKey1 DESC, Stock_Code",
                      ['NOTASTOCKCODE'])
        
(
    COLUMN_SORT1,
    COLUMN_STOCKCODE,
    COLUMN_NAME,
    COLUMN_SUPCODE,
    COLUMN_PRICE1,
    COLUMN_PRICE2,
    COLUMN_PRICE3,
    COLUMN_PRICE4
) = range(8)

def rowcheckfunc(model, path, iter, stockcode):
    global selected_path
    if model.get_value(iter,1) == stockcode:
        selected_path = path
                    
class PriceEditor(gtk.Window):
    def __init__(self, parent=None):
        gtk.Window.__init__(self)
        try:
            self.set_screen(parent.get_screen())
        except AttributeError:
            self.connect('destroy', lambda *w: gtk.main_quit())
        self.set_title("Price Editor")

        self.set_border_width(8)
        self.set_default_size(800,600)

        toprow = gtk.HBox(False, 8)
        
        self.searchbox1 = gtk.Entry()
        self.searchbox1.set_max_length(50)
        self.searchbox1.set_activates_default(True)
        self.searchbox1.set_text("Stock Code")
        
        self.searchbox2 = gtk.Entry()
        self.searchbox2.set_max_length(100)
        self.searchbox2.set_activates_default(True)
        self.searchbox2.set_text("Name")
        
        searchbtn = gtk.Button("Search")
        searchbtn.set_can_default(True)
        searchbtn.connect('clicked', self.search, self.searchbox1)
                
        toprow.pack_start(self.searchbox1, expand=True, fill=True)
        toprow.pack_start(self.searchbox2, expand=True, fill=True)
        toprow.pack_end(searchbtn, False, False, 0)
        self.searchbox1.show()
        self.searchbox2.show()
        searchbtn.show()

        toprow2 = gtk.HBox(False, 8)
        instructions = gtk.Label("Search for stock code, name or both. Use * as a wildcard to represent multiple characters.")
        toprow2.pack_start(instructions, True, True)
        instructions.show()

        vbox = gtk.VBox(False, 8)
        self.add(vbox)

        vbox.pack_start(toprow, False)
        vbox.pack_start(toprow2, False)

        sw = gtk.ScrolledWindow()
        sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        vbox.pack_start(sw)

        self.model = self.__create_model(rows)

        global treeview
        treeview = gtk.TreeView(self.model)
        treeview.set_rules_hint(True)
        treeview.set_search_column(COLUMN_STOCKCODE)

        global selected_path
        
        gtk.rc_parse_string( """
        style "custom-treestyle"{
            GtkTreeView::odd-row-color = "#ededed"
            GtkTreeView::even-row-color = "#ffffff"
            GtkTreeView::allow-rules = 1
        }
        widget "*custom_treeview*" style "custom-treestyle"
        """)
        treeview.set_name("custom_treeview" )

        sw.add(treeview)

        self.__add_columns(treeview)

        self.maximize()
        self.set_default(searchbtn)
        self.show_all()

    def search(self, widget, searchbox):
        searchterm1 = self.searchbox1.get_text()
        sqlsearchterm1 = searchterm1.replace("*","%")
        searchterm2 = self.searchbox2.get_text()
        sqlsearchterm2 = searchterm2.replace("*","%")
        qq_sqlsearchterm1 = "qq_" + sqlsearchterm1
        if (searchterm1 <> "" and searchterm1 <> "Stock Code") and (searchterm2 == "" or searchterm2 == "Name"):
            rows = cursor.execute("SELECT Stock_SortKey1, Stock_Code, Stock_Name, List(LITE_SUPSTOCK.PSS_SUPREF,', ') AS Supplier_Stock_Codes, Stock_PriceLine1, Stock_PriceLine2, Stock_PriceLine3, Stock_PriceLine4 FROM Stock_Records LEFT OUTER JOIN LITE_SUPSTOCK ON Stock_Records.Stock_Code=LITE_SUPSTOCK.PSS_STKCODE WHERE Stock_Inactive_Flag = 'NO' AND (Stock_Code LIKE ? OR Stock_Code LIKE ?) GROUP BY Stock_Code, Stock_SortKey1, Stock_Name, Stock_PriceLine1, Stock_PriceLine2, Stock_PriceLine3, Stock_PriceLine4 ORDER BY Stock_SortKey1 DESC, Stock_Code",
                                  [sqlsearchterm1, qq_sqlsearchterm1])
        if (searchterm1 == "" or searchterm1 == "Stock Code") and (searchterm2 <> "" and searchterm2 <> "Name"):
            rows = cursor.execute("SELECT Stock_SortKey1, Stock_Code, Stock_Name, List(LITE_SUPSTOCK.PSS_SUPREF,', ') AS Supplier_Stock_Codes, Stock_PriceLine1, Stock_PriceLine2, Stock_PriceLine3, Stock_PriceLine4 FROM Stock_Records LEFT OUTER JOIN LITE_SUPSTOCK ON Stock_Records.Stock_Code=LITE_SUPSTOCK.PSS_STKCODE WHERE Stock_Inactive_Flag = 'NO' AND Stock_Name LIKE ? GROUP BY Stock_Code, Stock_SortKey1, Stock_Name, Stock_PriceLine1, Stock_PriceLine2, Stock_PriceLine3, Stock_PriceLine4 ORDER BY Stock_SortKey1 DESC, Stock_Code",
                                  [sqlsearchterm2])
        if (searchterm1 <> "" and searchterm1 <> "Stock Code") and (searchterm2 <> "" and searchterm2 <> "Name"):
            rows = cursor.execute("SELECT Stock_SortKey1, Stock_Code, Stock_Name, List(LITE_SUPSTOCK.PSS_SUPREF,', ') AS Supplier_Stock_Codes, Stock_PriceLine1, Stock_PriceLine2, Stock_PriceLine3, Stock_PriceLine4 FROM Stock_Records LEFT OUTER JOIN LITE_SUPSTOCK ON Stock_Records.Stock_Code=LITE_SUPSTOCK.PSS_STKCODE WHERE Stock_Inactive_Flag = 'NO' AND (Stock_Code LIKE ? OR Stock_Code LIKE ?) AND Stock_Name LIKE ? GROUP BY Stock_Code, Stock_SortKey1, Stock_Name, Stock_PriceLine1, Stock_PriceLine2, Stock_PriceLine3, Stock_PriceLine4 ORDER BY Stock_SortKey1 DESC, Stock_Code",
                                  [sqlsearchterm1, qq_sqlsearchterm1, sqlsearchterm2])
        if (searchterm1 == "" or searchterm1 == "Stock Code") and (searchterm2 == "" or searchterm2 == "Name"):
            pass
        self.model = self.__create_model(rows)
        treeview.set_model(self.model)

    def __create_model(self, rows):
        lstore = gtk.ListStore(
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING,
            gobject.TYPE_STRING)

        lstore.clear()

        for item in rows:
            iter = lstore.append()
            if item[4] is None:
                item4 = 'None'
            if item[4] is not None:
                item4 = "%.2f" % item[4]
                
            if item[5] is None:
                item5 = 'None'
            if item[5] is not None:
                item5 = "%.2f" % item[5]
                
            if item[6] is None:
                item6 = 'None'
            if item[6] is not None:
                item6 = "%.2f" % item[6]

            if item[7] is None:
                item7 = 'None'
            if item[7] is not None:
                item7 = "%.2f" % item[7]
                
            lstore.set(iter,
                COLUMN_SORT1, item[0],
                COLUMN_STOCKCODE, item[1],
                COLUMN_NAME, item[2],
                COLUMN_SUPCODE, item[3],
                COLUMN_PRICE1, item4,
                COLUMN_PRICE2, item5,
                COLUMN_PRICE3, item6,
                COLUMN_PRICE4, item7)
        return lstore


        
    def cell_edited_callback(self, renderer, row, new_value, column, priceline):
        if not new_value.replace(".","").isdigit():
           pass 
           # message = gtk.MessageDialog(type=gtk.MESSAGE_ERROR)
           # message.set_markup("Error: Price contains letters.")
           # message.run()
        #if new_value.replace(".","").isdigit():
        if currencyre.match(new_value):
            stockcode = self.model[row][column]
            #print stockcode
            #print priceline
            #print float(new_value)
            current_value = self.model[row][priceline+3]
            if new_value <> current_value:
                cursor.execute("UPDATE STK_PRICE_KEYS SET SPL_PRICE = ? WHERE SPL_STKCODE = ? AND SPL_LINE = ?",
                               [float(new_value), stockcode, priceline])
                if priceline == 1:
                    cursor.execute("UPDATE F_SM1_STOCK SET STKL_SELLPRICE = ? WHERE STKL_STKCODE = ?",
                                    [float(new_value), stockcode])
                conn.commit()
                self.search('', self.searchbox1)
                self.model.foreach(rowcheckfunc, stockcode)
                columnobj = treeview.get_column(priceline+4)
                treeview.set_cursor(selected_path, columnobj, start_editing=True)
                
    def cell_edited_callback_name(self, renderer, row, new_value, column):
        if new_value == "":
            return
        stockcode = self.model[row][column]
        current_monname = self.model[row][2]

        if new_value <> current_monname:
            cursor.execute("UPDATE F_SM1_STOCK SET STKL_STKNAME = ? WHERE STKL_STKCODE = ?",
                           [new_value, stockcode])
            cursor.execute("DELETE FROM KEYWORD_SEARCH WHERE KWS_STKCODE = ?",
                           [stockcode])
            cursor.execute("exec mbo_stock_insert_keywords(?)",
                           [stockcode])
            conn.commit()
            self.search('', self.searchbox1)
            self.model.foreach(rowcheckfunc, stockcode)
            treeview.set_cursor(selected_path)
  
    def __add_columns(self,treeview):
        self.model = treeview.get_model()

        self.renderer1 = gtk.CellRendererText()
        self.renderer1.connect('edited', self.cell_edited_callback, 1, 1)
        self.renderer1.set_property('editable', True)

        self.renderer2 = gtk.CellRendererText()
        self.renderer2.connect('edited', self.cell_edited_callback, 1, 2)
        self.renderer2.set_property('editable', True)
        
        self.renderer3 = gtk.CellRendererText()
        self.renderer3.connect('edited', self.cell_edited_callback, 1, 3)
        self.renderer3.set_property('editable', True)
        
        self.renderer4 = gtk.CellRendererText()
        self.renderer4.connect('edited', self.cell_edited_callback, 1, 4)
        self.renderer4.set_property('editable', True)

        self.renderer5 = gtk.CellRendererText()
        self.renderer5.connect('edited', self.cell_edited_callback_name, 1)
        self.renderer5.set_property('editable', True)

        
        self.column1 = gtk.TreeViewColumn('Sort 1', gtk.CellRendererText(),
                                    text=COLUMN_SORT1)
        self.column1.set_sort_column_id(COLUMN_SORT1)
        treeview.append_column(self.column1)

        self.column2 = gtk.TreeViewColumn('Stock Code', gtk.CellRendererText(),
                                    text=COLUMN_STOCKCODE)
        self.column2.set_sort_column_id(COLUMN_STOCKCODE)
        treeview.append_column(self.column2)

        self.column3 = gtk.TreeViewColumn('Montana Long Name', self.renderer5,
                                    text=COLUMN_NAME)
        self.column3.set_sort_column_id(COLUMN_NAME)
        self.column3.set_resizable(True)
        treeview.append_column(self.column3)

        self.column4 = gtk.TreeViewColumn('Supplier Stock Codes', gtk.CellRendererText(),
                                    text=COLUMN_SUPCODE)
        self.column4.set_sort_column_id(COLUMN_SUPCODE)
        self.column4.set_resizable(True)
        treeview.append_column(self.column4)

        self.column5 = gtk.TreeViewColumn('Price 1', self.renderer1,
                                    text=COLUMN_PRICE1)
        self.column5.set_sort_column_id(COLUMN_PRICE1)
        treeview.append_column(self.column5)
        
        self.column6 = gtk.TreeViewColumn('Price 2', self.renderer2,
                                    text=COLUMN_PRICE2)
        self.column6.set_sort_column_id(COLUMN_PRICE2)
        treeview.append_column(self.column6)
        
        self.column7 = gtk.TreeViewColumn('Price 3', self.renderer3,
                                    text=COLUMN_PRICE3)
        self.column7.set_sort_column_id(COLUMN_PRICE3)
        treeview.append_column(self.column7)
        
        self.column8 = gtk.TreeViewColumn('Price 4', self.renderer4,
                                    text=COLUMN_PRICE4)
        self.column8.set_sort_column_id(COLUMN_PRICE4)
        treeview.append_column(self.column8)

def main():
    PriceEditor()
    gtk.main()

if __name__ == '__main__':
    main()

    


        
